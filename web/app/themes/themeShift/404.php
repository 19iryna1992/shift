<?php
/**
 * 404 Page template
 *
 * @package    WordPress
 * @subpackage theme_shift
 * @since      theme_shift 1.0
 */
get_header();
?>
<main id="page-content" class="page-content page-content--404">
	<div class="container">
		<div class="page-content__wrapper">
			<?php echo get_part('404/index'); ?>
		</div>
	</div>
</main>
<?php
get_footer();
