<?php
/**
 * Theme Footer
 *
 * @package    WordPress
 * @subpackage theme_shift
 * @since      theme_shift 1.0
 */
?>
			<?php echo get_part('components/footer/index'); ?>
			<?php wp_footer(); ?>
		</div> <!-- End of #page -->
	</body>
</html>
