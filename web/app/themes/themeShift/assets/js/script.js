document.body.addEventListener('mousedown', () => {
	document.body.classList.add('using-mouse');
});

document.body.addEventListener('keydown', event => {
	if (event.key === 'Tab') {
		document.body.classList.remove('using-mouse');
	}
});

window.addEventListener('load', () => {
	document.querySelector('body').classList.add('page-has-loaded');
	const timeoutDelay = document.body.classList.contains('changed-language') ? 0 : 3900;

	setTimeout(() => {
		if (document.querySelector('body').classList.contains('page-has-loaded')) {
			fadeUp();
		}
	}, timeoutDelay);
});


// animations

const fadeUp = () => {
	const items = document.querySelectorAll('.animation');
	const options = { rootMargin: '0px 0px -50px 0px' };

	const observer = new IntersectionObserver((entries, observer) => {
		entries.forEach(entry => {
			if (entry.isIntersecting) {
				entry.target.classList.add('is-visible');

				observer.unobserve(entry.target);
			}
		});
	}, options);

	items.forEach(item => {
		observer.observe(item);
	});
};