<?php

add_filter( 'body_class', function( $classes ) {
	if ( is_front_page() && ! empty( $_COOKIE['changed_language'] ) && $_COOKIE['changed_language'] === '1' ) {
		$classes[] = 'changed-language';
	}

	return $classes;
});