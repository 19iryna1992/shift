<?php 
function removeWidthHeight($string) {
    $string = preg_replace('/(width|height)="\d*"\s/', "", $string);
    
    if (strpos($string, 'vimeo.com') !== false) {
        $string = str_replace('" frameborder="0"', '&controls=0" frameborder="0"', $string);
    }

    return $string;
}