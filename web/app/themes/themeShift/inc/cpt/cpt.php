<?php

/**
 * Register Custom Post Type
 */
function register_cpt() {
	$all_post_types = apply_filters( 'theme_cpt', array() );

	if ( ! empty( $all_post_types ) ) {
		foreach ( $all_post_types as $args ) {
			$default_settings = array(
				'labels'             => array(
					'name'           => _x( $args['multiple_name'], 'post type general name', 'shift' ),
					'singular_name'  => _x( $args['singular_name'], 'post type singular name', 'shift' ),
					'menu_name'      => _x( $args['multiple_name'], 'admin menu', 'shift' ),
					'name_admin_bar' => _x( $args['singular_name'], 'add new on admin bar', 'shift' ),
					'all_items'      => $args['multiple_name'],
				),
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_rest'       => true,
				'has_archive'        => false,
				'exclude_from_search'=> false,
				'hierarchical'       => false,
				'rewrite'            => array( 'with_front' => false ),
				'query_var'          => true,
				'menu_position'      => 5,
				'show_in_menu'       => true,
				'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
			);

			$post_type_args = array_merge( $default_settings, $args );

			unset( $post_type_args['post_type'] );

			register_post_type( $args['post_type'], $post_type_args );

			if ( update_option( 'theme_cpt_' . $args['post_type'], true ) ) {
				flush_rewrite_rules();
			}
		}
	}
}
add_action( 'init', 'register_cpt' );


/**
 * Register Custom Taxonomies
 */
function register_taxonomies() {
	$all_taxonomies = apply_filters( 'theme_tax', array() );

	if ( ! empty( $all_taxonomies ) ) {
		foreach ( $all_taxonomies as $args ) {
			$default_settings = array(
				'labels'             => array(
					'name'          => _x( $args['multiple_name'], 'taxonomy general name', 'shift' ),
					'menu_name'     => __( $args['multiple_name'], 'shift' ),
					'search_items'  => __( 'Search ' . $args['multiple_name'], 'shift' ),
					'singular_name' => _x( $args['singular_name'], 'taxonomy singular name', 'shift' ),
					'all_items'     => $args['multiple_name'],
				),
				'hierarchical'       => true,
				'show_ui'            => true,
				'show_in_rest'       => true,
				'show_admin_column'  => true,
				'query_var'          => true,
				'publicly_queryable' => true,
				'rewrite'            => array( 'slug' => $args['tax_slug'], 'with_front' => false )
			);

			$tax_args = array_merge( $default_settings, $args );

			register_taxonomy( $args['tax_slug'], $args['for_types'], $tax_args );
		}
	}

	register_taxonomy_for_object_type( 'period', 'attachment');
}
add_action( 'init', 'register_taxonomies' );



/**
 * Custom Taxonomies Filter
 */
add_filter( 'theme_tax', function( $taxonomies ) {

	// Treatment Type
	$taxonomies[] = array(
		'tax_slug'      => 'section',
		'for_types'     => array( 'subdivisions' ),
		'singular_name' => 'Section',
		'multiple_name' => 'Section',
	);

	return $taxonomies;
} );


/**
 * Custom Post Type Filter
 */
add_filter( 'theme_cpt', function( $post_types ) {

	// Doctor
	$post_types[] = array(
		'post_type'     => 'subdivisions',
		'singular_name' => 'subdivisions',
		'multiple_name' => 'Subdivisions',
		'menu_icon'     => 'dashicons-layout',
		'supports'      => array( 'title', 'thumbnail'),
		'taxonomies'    => array( 'section'),
		'rewrite'       => array(
			'with_front' => false
		)
	);

	return $post_types;
} );