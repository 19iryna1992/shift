<?php
add_action('wp_ajax_floorplan_single', 'floorplan_single');
add_action('wp_ajax_nopriv_floorplan_single', 'floorplan_single');

function floorplan_single()
{
    $post_id = intval($_POST['pageId']);


    $args = [
        'post_type' => 'subdivisions',
        'post__in' => [$post_id],
    ];

    $query = new WP_Query($args);

    if ($query->have_posts()) :
        while ($query->have_posts()) : $query->the_post();
            $title = get_field('subdivisions_modal_title');
            $sub_title = get_field('subdivisions_modal_subtitle');
?>
            <?php if ( ! empty( $title ) || ! empty( $sub_title ) ) : ?>
                <div class="row">
                    <?php if (!empty($title)) : ?>
                        <div class="col-12">
                            <h2 class="floorplan-single__title">
                                <?php echo $title ?>
                            </h2>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($sub_title)) : ?>
                        <div class="col-12">
                            <p class="floorplan-single__subtitle">
                                <?php echo $sub_title ?>
                            </p>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php
                $options = [];
                $index = 0;

                if ( have_rows( 'options' ) ) :
                    while ( have_rows( 'options' ) ) : the_row();
                        $title = get_sub_field( 'title' );

                        if ( ! empty( $title ) ) :
                            $options[] = $title;
                        endif;
                    endwhile;
                endif;
            ?>

            <?php if ( have_rows( 'options' ) ) : ?>
                <div class="floorplan-single__options">
                    <?php while ( have_rows( 'options' ) ) : the_row(); ?>
                        <?php if ( ! empty( $title = get_sub_field( 'title' ) ) ) : ?>
                            <div class="floorplan-single__option row <?php echo $index == 0 ? 'active' : ''; ?>">
                                <div class="col-12 col-lg-4">
                                    <div class="floorplan-single__label">
                                        <?php echo $title; ?>
                                    </div>

                                    <?php if ( have_rows( 'items' ) ) : ?>
                                        <div class="floorplan-single__options-wraper">
                                            <?php while ( have_rows( 'items' ) ) : the_row();
                                                $item = get_sub_field( 'item' );

                                                if ( ! empty( $item ) ) :
                                            ?>
                                                <span class="floorplan-single__options-item">
                                                    <?php echo $item; ?>
                                                </span>
                                            <?php
                                                endif;
                                                endwhile;
                                            ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ( ! empty( $options ) ) : $i = 0; ?>
                                        <ul class="floorplan-single__list">
                                            <?php foreach( $options as $option ) : ?>
                                                <li class="floorplan-single__list-item">
                                                    <button>
                                                        <span class="floorplan-single__list-check">
                                                            <?php
                                                                if ( $options[$i] == $title ) {
                                                                    echo get_img('check');
                                                                }
                                                            ?>
                                                        </span>
                                                        <span><?php echo $option ?></span>
                                                    </button>
                                                </li>
                                            <?php $i += 1; endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </div>

                                <div class="col-12 col-lg-8">
                                    <?php if ( ! empty( $image = get_sub_field( 'image' ) ) ) : ?>
                                        <div class="floorplan-single__thumbnail">
                                            <?php echo get_img( $image ); ?>
                                        </div>

                                        <?php if ( have_rows( 'legend' ) ) : ?>
                                            <ul class="floorplan-single__legend">
                                            <?php while ( have_rows( 'legend' ) ) : the_row(); ?>
                                                <?php if ( ! empty( $text = get_sub_field( 'text' ) ) ) : ?>
                                                    <li class="floorplan-single__legend-item">
                                                        <?php if ( ! empty( $color = get_sub_field( 'color' ) ) ) : ?>
                                                            <span class="floorplan-single__legend-color" style="background-color: <?php echo $color; ?>"></span>
                                                        <?php endif; ?>

                                                        <span><?php echo $text; ?></span>
                                                    </li>
                                                <?php endif; ?>

                                            <?php endwhile;?>
                                            </ul>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php
                        $index += 1;
                        endwhile;
                    ?>
                </div>
            <?php endif; ?>

<?php endwhile;
        wp_reset_postdata();
        wp_die();
    endif;
}
