<?php

/**
 * Default Page template
 *
 * @package WordPress
 * @subpackage theme_shift
 * @since theme_shift 1.0
 */
get_header();
the_post();
?>
<main id="page-content" role="main" class="page-content page-content--default">
    <div id="content" tabindex="-1" class="page-content__wrapper">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="page-title <?php echo is_front_page() ? 'screen-reader-text' : '' ?>"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>

        <?php if (have_rows('page_content')) :
            while (have_rows('page_content')) : the_row();
                $module_name = get_row_layout();
                $module_class = str_replace('_', '-', $module_name);
                $style = get_sub_field('section_style');
                $section_id = !empty(get_sub_field('unique_section_id')) ? get_sub_field('unique_section_id') : 'section-' . unique_ID('module');
                $class = !empty($style) ? "$module_class style--$style" : $module_class;
        ?>
                <section id="<?php echo $section_id; ?>" class="<?php echo $class ?>">
                    <?php echo get_part('flexible-content/' . $module_name . '/index', ['class' => $module_name, 'section_style' => $style]); ?>
                </section>
        <?php
            endwhile;
        endif;
        ?>
    </div>
</main>
<?php
get_footer();
