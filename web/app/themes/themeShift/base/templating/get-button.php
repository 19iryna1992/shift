<?php

/**
 * Get theme button base on ACF Link field
 *
 * @param array ACF Link field
 * @param string Gutenberg style name
 * @param string additional custom class
 */
if ( ! function_exists( 'get_button' ) ) {

	function get_button( $link, $style = '', $custom_class = '' ) {
		$link_html = '';

		if ( ! empty( $link ) && is_array( $link ) ) {
			$link_url    = $link['section_link']['url'];
			$link_title  = $link['section_link']['title'];
			$link_target = $link['section_link']['target'] ? $link['section_link']['target'] : '_self';
			$link_modal = isset($link['modal']) ? $link['modal'] : false;


			$args = [
				'link_url'    => $link_url,
				'link_title'  => $link_title,
				'link_target' => $link_target,
				'style'       => $style,
				'set_class'   => $custom_class,
				'section_icon_position' => $link['section_icon_position'],
				'section_icon' => $link['section_icon'],
				'section_button_color' => $link['section_button_color'],
				'modal_button' => $link_modal,
			];

			$link_html = get_part( 'components/button/index', $args );
		}

		return $link_html;
	}

}