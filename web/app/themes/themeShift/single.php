<?php

/**
 * Single Blog Post template.
 *
 * @package    WordPress
 * @subpackage theme_shift
 * @since      theme_shift 1.0
 */
get_header();
the_post();
?>
<main id="page-content" role="main" class="page-content page-content--single">
	<div id="content" tabindex="-1" class="page-content__wrapper">
		<?php the_content(); ?>
		<?php
		if (have_rows('page_content')) :
			while (have_rows('page_content')) : the_row();
				$module_name = get_row_layout();
				$module_class = str_replace('_', '-', $module_name);
				$style = get_sub_field('section_style');
				$class = !empty($style) ? "$module_class style--$style" : $module_class;
		?>
				<section id="section-<?php echo unique_ID('module') ?>" class="<?php echo $class ?>">
					<?php echo get_part('flexible-content/' . $module_name . '/index', ['class' => $module_name, 'section_style' => $style]); ?>
				</section>
		<?php
			endwhile;
		endif;
		?>
	</div>
</main>
<?php
get_footer();
