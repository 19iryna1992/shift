<?php
/**
 * Flexible Content: Business
 *
 * @package    WordPress
 * @subpackage theme_shift
 * @since      theme_shift 1.0
 */

 //settings
$background_color = get_sub_field('background_color') ?: '#FF704E';
$headers_color = get_sub_field('headers_color') ?: '#F6F7D3';

//classes
$classes = [];
get_sub_field('spacing_top') ? $classes[] = 'spacing-top' : null;
get_sub_field('spacing_bottom') ? $classes[] = 'spacing-bottom' : null;
get_sub_field('module_size') ? $classes[] = 'business__wrapper--'.get_sub_field('module_size') : null;

//content
$left_title = get_sub_field('left_title') ?: '';
$right_upper_title = get_sub_field('right_upper_title') ?: '';
$right_title = get_sub_field('right_title') ?: '';
$right_text = get_sub_field('right_text') ?: '';
$left_image = get_sub_field('left_image') ?: '';
$right_image = get_sub_field('right_image') ?: '';
$left_image_mobile = get_sub_field('left_image_mobile') ?: '';
$right_image_mobile = get_sub_field('right_image_mobile') ?: '';

if(get_sub_field('module_size') === 'wide') {
    $left_image ? $left_image = get_img($left_image, 'module-2-wide-left') : [];
    $left_image_mobile ? $left_image_mobile = get_img($left_image_mobile, 'module-2-wide-left--mobile') : [];
    $right_image ? $right_image = get_img($right_image, 'module-2-wide-right') : [];
    $right_image_mobile ? $right_image_mobile = get_img($right_image_mobile, 'module-2-wide-right--mobile') : [];
} else {
    $left_image ? $left_image = get_img($left_image, 'module-2-narrow-left') : [];
    $left_image_mobile ? $left_image_mobile = get_img($left_image_mobile, 'module-2-narrow-left--mobile') : [];
    $right_image ? $right_image = get_img($right_image, 'module-2-narrow-right') : [];
    $right_image_mobile ? $right_image_mobile = get_img($right_image_mobile, 'module-2-narrow-right--mobile') : [];
}
?>

<div class="business__wrapper <?php echo implode(' ', $classes); ?>" style="background-color: <?php echo $background_color ?>;">
    <?php load_inline_styles(__DIR__, 'business'); ?>
    <div class="business__header">
        <h2 class="business__title" style="color: <?php echo $headers_color ?>;">
            <span class="animation fade-up">
                <span class="animation-container"><?php echo $left_title ?></span>
            </span>
        </h2>
        <?php if($right_image): ?>
        <div class="business__image-wrapper business__image-wrapper--right img__wrapper animation animation__fade-left">
            <div class="img__wrapper img__wrapper--desktop<?php echo $right_image_mobile ? null : ' img__wrapper--no-mobile'; ?>">
                <?php echo $right_image ?>
            </div>
            <div class="img__wrapper img__wrapper--mobile">
                <?php echo $right_image_mobile ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <div class="business__content">
        <?php if($left_image): ?>
        <div class="business__image-wrapper business__image-wrapper--left img__wrapper animation animation__fade-right">
            <div class="img__wrapper img__wrapper--desktop<?php echo $left_image_mobile ? null : ' img__wrapper--no-mobile'; ?>">
                <div><?php echo $left_image ?></div>
            </div>
            <div class="img__wrapper img__wrapper--mobile">
                <div><?php echo $left_image_mobile ?></div>
            </div>
        </div>
        <?php endif; ?>
        <div class="business__text">
            <?php if($right_upper_title): ?>
                <div class="business__upper-title">
                    <span class="animation fade-up">
                        <span class="animation-container"><?php echo $right_upper_title ?></span>
                    </span>
                </div>
            <?php endif; ?>
            <?php if($right_title): ?>
                <h3 class="business__title--text" style="color: <?php echo $headers_color ?>;">
                    <span class="animation fade-up">
                        <span class="animation-container"><?php echo $right_title ?></span>
                    </span>
                </h3>
            <?php endif; ?>
            <?php if($right_text): ?>
                <div class="business__description">
                    <span class="animation fade-up">
                        <span class="animation-container"><?php echo $right_text ?></span>
                    </span>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>