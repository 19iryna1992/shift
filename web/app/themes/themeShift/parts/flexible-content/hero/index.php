<?php
$title = get_sub_field('title') ?: '';
$title_tag = get_sub_field('title_tag') ?: '';
$sub_title = get_sub_field('upper_title') ?: '';
$button_type = get_sub_field('button_type') ?: '';
$button_label = get_sub_field('button_label') ?: '';
$title_size = get_sub_field('title_size') ?: '';

$first_image = get_sub_field('first_image') ? get_img(get_sub_field('first_image'), 'hero-top') : '';
$first_image_mobile = get_sub_field('first_image') ? get_img(get_sub_field('first_image'), 'hero-top') : '';

$second_image = get_sub_field('second_image') ? get_img(get_sub_field('second_image'), 'large') : '';
$second_image_mobile = get_sub_field('second_image') ? get_img(get_sub_field('second_image'), 'large') : '';

$third_image = get_sub_field('third_image') ? get_img(get_sub_field('third_image'), 'hero-bottom') : '';
$third_image_mobile = get_sub_field('third_image') ? get_img(get_sub_field('third_image'), 'hero-bottom') : '';

if($button_type === 'scroll') {
    $button_label = get_sub_field('button_label') ?: 'Scroll';
    $button_destination = get_sub_field('scroll_section_id') ?: '#section-2';
} else {
    $button = get_sub_field('button') ?: [];
    $button = $button['section_button'];
    get_sub_field('modal_shitcher') ? $button['modal'] = true : null;
}
get_sub_field('modal_shitcher') ? load_inline_styles_shared('forms') : null;
load_inline_styles(__DIR__, 'hero');
?>

<div class="hero__wrapper">
    <?php if($first_image): ?>
    <div class="hero__top">
        <div class="hero__img hero__img--top img__wrapper animation animation__fade-right">
            <div class="img__wrapper img__wrapper--desktop"><div><?php echo $first_image; ?></div></div>
            <div class="img__wrapper img__wrapper--mobile"><div><?php echo $first_image_mobile; ?></div></div>
        </div>
    </div>
    <?php endif; ?>
    <div class="hero__middle<?php echo $title_size === 'big' ? ' hero__middle--big-title' : null; ?>">
        <div class="hero__content">
            <?php if($sub_title): ?>
                <p class="hero__title--upper">
                    <span span class="animation fade-up">
                        <span class="animation-container"><?php echo $sub_title; ?></span>
                    </span>
                </p>
            <?php endif; ?>
            <?php if($title): ?>
                <<?php echo $title_tag ? $title_tag : 'h2' ?> class="hero__title hero__title--<?php echo $title_size; ?>">
                    <span span class="animation fade-up">
                        <span class="animation-container"><?php echo $title; ?></span>
                    </span>
                <<?php echo $title_tag ? $title_tag : 'h2' ?>/>
            <?php endif; ?>
        </div>
    <?php if($second_image): ?>
        <div class="hero__img hero__img--middle img__wrapper animation animation__fade-left">
            <div class="img__wrapper img__wrapper--desktop"><?php echo $second_image; ?></div>
            <div class="img__wrapper img__wrapper--mobile"><?php echo $second_image_mobile; ?></div>
        </div>
    <?php endif; ?>
    </div>
    <div class="hero__bottom">
    <?php if($third_image): ?>
        <div class="hero__img hero__img--bottom img__wrapper animation animation__fade-right">
            <div class="img__wrapper img__wrapper--desktop"><?php echo $third_image; ?></div>
            <div class="img__wrapper img__wrapper--mobile"><?php echo $third_image_mobile; ?></div>
        </div>
    <?php endif; ?>
    <?php if($button_type === 'scroll'): ?>
            <a class="scroll_link" href="#<?php echo $button_destination; ?>" target="_self">
            <span span class="animation fade-up">
                <span class="animation-container">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M16 8L14.59 6.59L9 12.17V0H7V12.17L1.42 6.58L0 8L8 16L16 8Z" fill="#1F3538"/>
                </svg>
                <?php echo $button_label; ?>
                </span>
            </span>
            </a>
    <?php else: ?>
        <?php echo get_button($button); ?>
    <?php endif; ?>
    </div>
</div>

<?php
    if(get_sub_field('modal_shitcher')) {
        $modal_data['header'] = get_sub_field('header') ?: '';
        $modal_data['image'] = get_sub_field('image') ?: '';
        $modal_data['form'] = get_sub_field('form_shortcode') ?: '';
        $modal_data['file'] =  get_field('download_popup_file', 'options') ?: '';

        get_part('components/modal/index', $modal_data);
    }
?>
