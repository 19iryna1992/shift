<?php
$title              = get_sub_field('title');
$sub_title          = get_sub_field('upper_title');
$button_label       = get_sub_field('section_button');
$subdivisions_posts = get_sub_field('subdivisions_posts');
$image_id           = get_sub_field('default_image');

get_sub_field('modal_shitcher') ? $button_label['modal'] = true : null;

$sections         = [];
$subdivisions_ids = [];

if ($subdivisions_posts) {
	foreach ($subdivisions_posts as $subdivisions_post) {
		$current_terms = wp_get_post_terms($subdivisions_post->ID, 'section', array('fields' => 'ids'));

		if (!in_array($subdivisions_post->ID, $subdivisions_ids)) {
			$subdivisions_ids[] += $subdivisions_post->ID;
		}

		foreach ($current_terms as $term) {
			if (!in_array($term, $sections)) {
				$sections[] += $term;
			}
		}
	}
}

sort($sections);

load_inline_styles(__DIR__, 'floorplan');
load_blocks_script('floorplan', 'shift/floorplan', [], 'parts/flexible-content');
?>

<div class="container">
	<header class="floorplan__header">
		<div class="row">
			<div class="col-12">
				<?php if (!empty($sub_title)) : ?>
					<p class="floorplan__section-sub-title">
						<?php echo $sub_title ?>
					</p>
				<?php endif ?>
				<?php if (!empty($title)) : ?>
					<h2 class="floorplan__section-title">
						<?php echo $title ?>
					</h2>
				<?php endif ?>
			</div>
			<?php if (!empty($button_label)) : ?>
				<div class="project__button">
					<span class="animation-container">
						<?php echo get_button($button_label); ?>
					</span>
				</div>
			<?php endif; ?>
		</div>
	</header>

	<div class="row">
		<div class="col-12 col-lg-6">
			<div class="floorplan__tab-image ">
				<div class="floorplan__tab-image--default js--tab-img-default show">
					<?php echo get_img('shift_building'); ?>
				</div>
			</div>
		</div>
		<div class="col-12 col-lg-6">
			<div class="floorplan__tab-box">
				<?php if (!empty($sections)) :
					$filter_icon = 'filter';
				?>
					<?php echo get_img($filter_icon, 'full') ?>

					<button class="floorplan__tab-btn floorplan__tab-btn--all js--tab-btn active"><?php echo __('TOUT', 'shift') ?></button>

					<?php foreach ($sections as $section) :
						$section_term = get_term_by('id', $section, 'section');
					?>
						<button class="floorplan__tab-btn js--tab-btn" data-term="<?php echo $section_term->name; ?>">
							<?php echo $section_term->name ?>
						</button>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>

			<?php if (!empty($sections)) :
				$subdivisions_all_args = array(
					'post_type'      => 'subdivisions',
					'post__in'       => $subdivisions_ids,
					'orderby'        => 'post__in',
					'posts_per_page' => -1
				);

				$query_all = new WP_Query($subdivisions_all_args);
			?>
				<div class="floorplan__table">
					<table class="floorplan__tab-content-box">
						<thead>
							<tr>
								<th class="floorplan__term-name"> <span><?php echo __('BÂT', 'shift') ?></span></th>
								<th class="cell-2"><?php echo __('Étage', 'shift') ?></th>
								<th class="cell-3"><?php echo __('m2', 'shift') ?></th>
								<th class="cell-4"><?php echo __('Statut', 'shift') ?></th>
								<th class="border-bottom"><?php echo __('', 'shift') ?></th>
							</tr>
						</thead>

						<?php if ($query_all->have_posts()) : ?>
							<tbody class="floorplan__tab-content floorplan__tab-content--all js--tab-content active">
								<?php while ($query_all->have_posts()) : $query_all->the_post();
									$post_id = get_the_ID();
									$terms   = get_the_terms($post_id, 'section');
									$floor   = get_field('floor');
									$area    = get_field('area');
									$status  = get_field('status');
									$icon    = 'eye';

									$terms_classes = [];

									if ($terms && is_array($terms)) {
										foreach ($terms as $term) {
											$terms_classes[] = $term->name;
										};
									}
								?>
									<tr>
										<td class="floorplan__term-name js--term-name <?php echo (!empty($terms_classes)) ? implode(" ", $terms_classes) : ''; ?>">
											<?php if ($terms && is_array($terms) && count($terms) == 1) : ?>
												<?php foreach ($terms as $term) : ?>
													<?php echo $term->name; ?>
												<?php endforeach ?>
											<?php elseif ($terms && is_array($terms) && count($terms) > 1) :
												$terms_count = count($terms);
												$i = 1;
											?>
												<?php echo __('Passerelle bâtiments ', 'shift') ?>
												<?php foreach ($terms as $term) : ?>
													<?php echo $term->name; ?>
												<?php
													if ($i !== $terms_count) :
														echo __('et', 'shift');
													endif;
													++$i;
												endforeach ?>
											<?php endif ?>
										</td>

										<td class="cell cell-2">
											<?php echo $floor; ?>
										</td>

										<td class="cell cell-3">
											<?php if (!empty($area)) : ?>
												<?php echo $area; ?>
											<?php endif ?>
										</td>

										<td class="cell cell-4">
											<?php if (!empty($status)) : ?>
												<?php echo $status; ?>
											<?php endif ?>
										</td>

										<td class="cell-5">
											<a href="javascript:void(0)" class="floorplan__open-floorplan js--open-floorplan" data-page-id="<?php echo $post_id ?>">
												<?php echo get_img($icon, 'full'); ?>
											</a>
										</td>
									<tr>

									<?php endwhile; ?>
							</tbody>
						<?php endif; ?>

						<?php wp_reset_postdata(); ?>

						<?php foreach ($sections as $section) :
							$section_term = get_term_by('id', $section, 'section');

							$subdivisions_args = array(
								'post_type' => 'subdivisions',
								'post__in' => $subdivisions_ids,
								'orderby' => 'post__in',
								'tax_query' => array(
									array(
										'taxonomy' => 'section',
										'field' => 'term_id',
										'terms' => $section
									)
								)
							);

							$query = new WP_Query($subdivisions_args);
						?>
							<?php if ($query->have_posts()) : ?>
								<tbody class="floorplan__tab-content js--tab-content">
									<?php while ($query->have_posts()) : $query->the_post();
										$post_id = get_the_ID();
										$floor   = get_field('floor');
										$area    = get_field('area');
										$status  = get_field('status');
										$icon    = 'eye';
									?>
										<tr>
											<td class="floorplan__term-name">
												<?php echo $section_term->name; ?>
											</td>

											<td class="cell-2">
												<?php echo $floor; ?>
											</td>

											<td class="cell-3">
												<?php if (!empty($area)) : ?>
													<?php echo $area; ?>
												<?php endif ?>
											</td>

											<td class="cell-4">
												<?php if (!empty($status)) : ?>
													<?php echo $status; ?>
												<?php endif ?>
											</td>

											<td class="cell-5">
												<a href="javascript:void(0)" class=" floorplan__open-floorplan js--open-floorplan" data-page-id="<?php echo $post_id ?>">
													<?php echo get_img($icon, 'full'); ?>
												</a>
											</td>
										<tr>
										<?php endwhile; ?>
								</tbody>
							<?php endif; ?>

							<?php wp_reset_postdata(); ?>

						<?php endforeach; ?>
					</table>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php

if (get_sub_field('modal_shitcher')) {
	$modal_data['header'] = get_sub_field('header') ?: '';
	if (is_array(get_sub_field('image'))) {
		$img = get_sub_field('image');
		$modal_data['image'] = $img['ID'] ?: '';
	} else {
		$modal_data['image'] = get_sub_field('image') ?: '';
	}
	$modal_data['form'] = get_sub_field('form_shortcode') ?: '';
	$modal_data['file'] = get_field('surfaces_popup_file', 'options') ?: '';

	get_part('components/modal/index', $modal_data);
}

echo get_part('flexible-content/floorplan/floorplan-single');
?>