/* global vars */

const tabBtns = document.querySelectorAll('.js--tab-btn');
const tabContentAll = document.querySelectorAll('.js--tab-content');
const svgPath = document.querySelectorAll('.bg');
const terms = document.querySelectorAll('.js--term-name');
const cells = document.querySelectorAll('.cell');

let activeTerms = [];

const fillSvg = terms => {
	svgPath.forEach(path => {
		if (terms.length === 0) {
			path.style.pointerEvents = '';
			path.style.fill = '';
		} else {
			path.style.pointerEvents = 'none';
			const { letter } = path.dataset;

			if (terms.includes(letter)) {
				path.style.fill = '#FF704E';
			} else {
				path.style.fill = '';
			}
		}
	});
};

const removeActiveClass = elems => {
	elems.forEach(el => el.classList.remove('active'));
};

tabBtns.forEach((btn, index) => {
	btn.addEventListener('click', () => {
		if (btn.classList.contains('floorplan__tab-btn--all')) {
			activeTerms = [];
			fillSvg(activeTerms);

			removeActiveClass(tabBtns);
			removeActiveClass(tabContentAll);

			tabBtns[0].classList.add('active');
			tabContentAll[0].classList.add('active');
		} else {
			const { term } = btn.dataset;

			if (activeTerms.includes(term)) {
				const indexToRemove = activeTerms.indexOf(term);
				activeTerms.splice(indexToRemove, 1);
			} else {
				activeTerms.push(term);
			}

			fillSvg(activeTerms);

			let activeItemsCounter = 0;

			btn.classList.toggle('active');
			tabContentAll[index].classList.toggle('active');

			tabBtns[0].classList.remove('active');
			tabContentAll[0].classList.remove('active');

			tabBtns.forEach(el => {
				if (el.classList.contains('active')) {
					activeItemsCounter += 1;
				}
			});

			if (activeItemsCounter === 0) {
				tabBtns[0].classList.add('active');
				tabContentAll[0].classList.add('active');
			}
		}
	});
});

const events = ['mouseover', 'click'];

svgPath.forEach(path => {
	events.forEach(event => {
		path.addEventListener(event, () => {
			const termName = path.getAttribute('data-letter');
			const allItems = document.querySelectorAll(`.${termName}`);

			allItems.forEach(item => {
				item.style.backgroundColor = '#FF704E';
			});

			path.style.fill = '#FF704E';
		});

		path.addEventListener('mouseout', () => {
			const termName = path.getAttribute('data-letter');
			const allItems = document.querySelectorAll(`.${termName}`);

			allItems.forEach(item => {
				item.style.background = '';
			});

			path.style.fill = '';
		});
	});
});

terms.forEach(term => {
	events.forEach(event => {
		term.addEventListener(event, () => {
			svgPath.forEach(path => {
				if (term.classList.contains(path.getAttribute('data-letter'))) {
					path.style.fill = '#FF704E';
				}
			});

			term.style.backgroundColor = '#FF704E';
		});
	});

	term.addEventListener('mouseout', () => {
		svgPath.forEach(path => {
			if (term.classList.contains(path.getAttribute('data-letter'))) {
				path.style.fill = '';
			}
		});

		term.style.backgroundColor = '';
	});
});

cells.forEach(cell => {
	const parent = cell.parentElement.querySelector('.floorplan__term-name');

	events.forEach(event => {
		cell.addEventListener(event, () => {
			if (event !== 'click') {
				parent.click();
			}
		});
	});

	cell.addEventListener('mouseout', () => {
		svgPath.forEach(path => {
			path.style.fill = '';
		});

		parent.style.background = '';
	});
});


/* ---------------------------------
	Modal Window
---------------------------------*/

const openModalBtn = document.querySelectorAll('.js--open-floorplan');
const modal = document.querySelector('.js--floorplan-single');
const closeModalBtn = document.querySelector('.js--floorplan-single-close');
const modalContent = document.querySelector('.js--floorplan-single-content');

const toggleOptions = (options, index) => {
	options.forEach(option => {
		option.classList.remove('active');
	});

	options[index].classList.add('active');
};

const bindEvents = options => {
	options.forEach(option => {
		const buttons = option.querySelectorAll('.floorplan-single__list button');


		buttons.forEach((button, index) => {
			button.addEventListener('click', () => {
				toggleOptions(options, index);
			});
		});
	});
};

closeModalBtn.addEventListener('click', () => {
	if (modal.classList.contains('active')) {
		modal.classList.remove('active');
		document.body.classList.remove('modal-open');
		modalContent.innerHTML = '';
	}
});

openModalBtn.forEach(modalBtn => {
	modalBtn.addEventListener('click', () => {
		const formData = new FormData();

		document.body.classList.add('modal-open');

		if (modal.classList.contains('active') !== true) {
			modal.classList.add('active', 'loading');
		}

		formData.append('action', 'floorplan_single');
		formData.append('pageId', modalBtn.dataset.pageId);

		fetch(vars.ajaxUrl, {
			method: 'POST',
			body: formData,
		})
			.then(response => response.text())
			.then(data => {
				modalContent.innerHTML = data;
				modal.classList.remove('loading');

				bindEvents(modalContent.querySelectorAll('.floorplan-single__option'));
			})
			.catch(error => console.error(error));
	});
});