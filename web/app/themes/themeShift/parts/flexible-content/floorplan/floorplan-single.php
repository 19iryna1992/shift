   <div class="floorplan-single js--floorplan-single">
       <div class="container">
           <div class="floorplan-single__content">
               <button class="floorplan-single__close js--floorplan-single-close">
                   <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                       <path d="M12.8 38L10 35.2L21.2 24L10 12.8L12.8 10L24 21.2L35.2 10L38 12.8L26.8 24L38 35.2L35.2 38L24 26.8L12.8 38Z" fill="#1F3538" />
                   </svg>
               </button>
               <div class="js--floorplan-single-content">
               </div>

               <div class="floorplan-single__loader">
                    <?php echo get_img( 'loader' ); ?>
               </div>
           </div>
       </div>
   </div>