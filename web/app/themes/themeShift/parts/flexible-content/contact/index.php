<?php
$upper_title = get_sub_field('upper_title') ?: '';
$title = get_sub_field('title') ?: '';
$logo = get_sub_field('logo') ? get_img(get_sub_field('logo')) : '';
$image = get_sub_field('image') ? get_img(get_sub_field('image')) : '';
$name = get_sub_field('name') ?: '';
$function = get_sub_field('function') ?: '';

$button = get_sub_field('section_button') ?: '';

$cta_content = get_sub_field('cta_content') ?: '';
$contact_form = get_sub_field('contact_form') ?: '';

?>

<div class="container">
    <?php
        load_blocks_script('contact', 'shift/contact', '', 'parts/flexible-content'); 
        load_inline_styles(__DIR__, 'contact'); 
    ?>
    <div class="contact__data">
        <div class="contact__header">
        <?php if($upper_title): ?>
            <p class="contact__title--upper">
                <span class="animation fade-up">
                    <span class="animation-container"><?php echo $upper_title ?></span>
                </span>
            </p>
        <?php endif; ?>
        <?php if($title): ?>
            <h2 class="contact__title">
                <span class="animation fade-up">
                    <span class="animation-container"><?php echo $title ?></span>
                </span>
            </h2>
        <?php endif; ?>
        </div>
        <div class="contact__person animation animation__fade-right">
        <?php if($logo): ?>
            <div class="contact__logo">
                <?php echo $logo ?>
            </div>
        <?php endif; ?>
        <div class="contact__personal-data">
            <?php if($image): ?>
                <div class="contact__image">
                    <?php echo $image ?>
                </div>
            <?php endif; ?>
            <?php if($name): ?>
                <div class="contact__name">
                    <span class="animation fade-up">
                        <span class="animation-container"><?php echo $name ?></span>
                    </span>
                </div>
            <?php endif; ?>
            <?php if($function): ?>
                <div class="contact__function">
                    <span class="animation fade-up">
                        <span class="animation-container"><?php echo $function ?></span>
                    </span>
                </div>
            <?php endif; ?>
            <?php if($cta_content): ?>
                <div class="contact__hidden--data">
                    <?php echo $button ? get_button($button) : ''; ?>

                    <div class="contact__hidden--wrapper">
                        <div class="contact__hidden--inner">
                    <?php foreach($cta_content as $contact): ?>
                        <?php if($contact['content_type'] === 'link'): ?>
                            <div class="contact__hidden--link">
                                <a href="<?php echo $contact['link_data']['url']; ?>" target="<?php echo $contact['link_data']['target']; ?>">
                                    <?php echo $contact['link_data']['title']; ?>
                                </a>
                            </div>
                        <?php else: ?>
                            <div class="contact__hidden--text">
                                <?php echo $contact['text_data']; ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            </div>
        </div>
    </div>
<?php if($contact_form): ?>
    <div class="contact__form animation animation__fade-left">
        <?php echo do_shortcode($contact_form); ?>
    </div>
<?php endif; ?>
</div>