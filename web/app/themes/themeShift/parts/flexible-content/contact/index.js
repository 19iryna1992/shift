const buttons = document.querySelectorAll('.contact__hidden--data .wp-block-button__link');

const showHiddenData = trigger => {
	const hiddenContainer = trigger.closest('.contact__hidden--data');
	const hiddenWrapper = hiddenContainer.querySelector('.contact__hidden--wrapper');
	const height = hiddenContainer.querySelector('.contact__hidden--inner').offsetHeight;

	hiddenWrapper.style.height = `${height}px`;
	trigger.remove();
};

buttons.forEach(button => {
	button.addEventListener('click', e => {
		e.preventDefault();
		showHiddenData(e.currentTarget);
	});
});
