<?php
$title = get_sub_field('title_left');
$text = get_sub_field('text_left');
$image_right = get_sub_field('image_right') ? get_img(get_sub_field('image_right')) : '';
$slider = get_sub_field('slider');




load_inline_styles_plugin('splide.min');
load_inline_styles_shared('sliders');
load_blocks_script('lively_neighborhood', 'shift/lively-neighborhood', ['splide'], 'parts/flexible-content');


?>

<div class="lively-neighborhood__wrapper">
    <?php load_inline_styles(__DIR__, 'lively-neighborhood'); ?>
    <div class="lively-neighborhood__top">
        <div class="lively-neighborhood__header">
            <?php if ($title) : ?>
                <h2 class="lively-neighborhood__title">
                    <span span class="animation fade-up">
                        <span class="animation-container">
                            <?php echo $title; ?>
                        </span>
                    </span>
                </h2>
            <?php endif ?>
            <?php if ($text) : ?>
                <div class="lively-neighborhood__text">
                    <span span class="animation fade-up">
                        <span class="animation-container">
                            <?php echo $text ?>
                        </span>
                    </span>
                </div>
            <?php endif ?>
        </div>
    <?php if ($image_right) : ?>
        <div class="lively-neighborhood__top-image-wrapper img__wrapper animation animation__fade-left">
            <div class="img__wrapper img__wrapper--desktop">
                <?php echo $image_right ?>
            </div>
        </div>
    <?php endif ?>
    </div>
    <div class="lively-neighborhood__bottom">
    <?php if($slider): ?>
        <div class="lively-neighborhood__splide splide">
            <div class="lively-neighborhood__track splide__track">
                <div class="lively-neighborhood__list splide__list">
                    <?php foreach($slider as $image): ?>
                        <?php
                            $image_desktop = get_img($image, 'optimisation');
                            $image_mobile = get_img($image, 'optimisation--mobile');
                        ?>
                        <div class="lively-neighborhood__slide splide__slide">
                            <div class="lively-neighborhood__image-wrapper img__wrapper">
                                <div class="lively-neighborhood__image-wrapper--desktop img__wrapper--desktop">
                                    <?php echo $image_desktop; ?>
                                </div>
                                <div class="lively-neighborhood__image-wrapper--mobile img__wrapper--mobile">
                                    <?php echo $image_mobile; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="lively-neighborhood__control">
                <ul class="lively-neighborhood__pagination splide__pagination"></ul>
                <div class="lively-neighborhood__arrows splide__arrows">
                    <button class="splide__arrow splide__arrow--prev">
                        <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M28.25 38.25L21.75 31.75C21.2917 31.2917 21.0625 30.7083 21.0625 30C21.0625 29.2917 21.2917 28.7083 21.75 28.25L28.3125 21.6875C28.7708 21.2292 29.3433 21.01 30.03 21.03C30.7183 21.0517 31.2917 21.2917 31.75 21.75C32.2083 22.2083 32.4375 22.7917 32.4375 23.5C32.4375 24.2083 32.2083 24.7917 31.75 25.25L29.5 27.5H37.5625C38.2708 27.5 38.8542 27.7392 39.3125 28.2175C39.7708 28.6975 40 29.2917 40 30C40 30.7083 39.76 31.3017 39.28 31.78C38.8017 32.26 38.2083 32.5 37.5 32.5H29.5L31.8125 34.8125C32.2708 35.2708 32.49 35.8442 32.47 36.5325C32.4483 37.2192 32.2083 37.7917 31.75 38.25C31.2917 38.7083 30.7083 38.9375 30 38.9375C29.2917 38.9375 28.7083 38.7083 28.25 38.25ZM30 55C33.4583 55 36.7083 54.3433 39.75 53.03C42.7917 51.7183 45.4375 49.9375 47.6875 47.6875C49.9375 45.4375 51.7183 42.7917 53.03 39.75C54.3433 36.7083 55 33.4583 55 30C55 26.5417 54.3433 23.2917 53.03 20.25C51.7183 17.2083 49.9375 14.5625 47.6875 12.3125C45.4375 10.0625 42.7917 8.28083 39.75 6.9675C36.7083 5.65583 33.4583 5 30 5C26.5417 5 23.2917 5.65583 20.25 6.9675C17.2083 8.28083 14.5625 10.0625 12.3125 12.3125C10.0625 14.5625 8.28167 17.2083 6.97 20.25C5.65667 23.2917 5 26.5417 5 30C5 33.4583 5.65667 36.7083 6.97 39.75C8.28167 42.7917 10.0625 45.4375 12.3125 47.6875C14.5625 49.9375 17.2083 51.7183 20.25 53.03C23.2917 54.3433 26.5417 55 30 55Z" fill="#FF704E"/>
                        </svg>
                    </button>
                    <button class="splide__arrow splide__arrow--next">
                        <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M31.75 38.25L38.25 31.75C38.7083 31.2917 38.9375 30.7083 38.9375 30C38.9375 29.2917 38.7083 28.7083 38.25 28.25L31.6875 21.6875C31.2292 21.2292 30.6567 21.01 29.97 21.03C29.2817 21.0517 28.7083 21.2917 28.25 21.75C27.7917 22.2083 27.5625 22.7917 27.5625 23.5C27.5625 24.2083 27.7917 24.7917 28.25 25.25L30.5 27.5H22.4375C21.7292 27.5 21.1458 27.7392 20.6875 28.2175C20.2292 28.6975 20 29.2917 20 30C20 30.7083 20.24 31.3017 20.72 31.78C21.1983 32.26 21.7917 32.5 22.5 32.5H30.5L28.1875 34.8125C27.7292 35.2708 27.51 35.8442 27.53 36.5325C27.5517 37.2192 27.7917 37.7917 28.25 38.25C28.7083 38.7083 29.2917 38.9375 30 38.9375C30.7083 38.9375 31.2917 38.7083 31.75 38.25ZM30 55C26.5417 55 23.2917 54.3433 20.25 53.03C17.2083 51.7183 14.5625 49.9375 12.3125 47.6875C10.0625 45.4375 8.28167 42.7917 6.97 39.75C5.65667 36.7083 5 33.4583 5 30C5 26.5417 5.65667 23.2917 6.97 20.25C8.28167 17.2083 10.0625 14.5625 12.3125 12.3125C14.5625 10.0625 17.2083 8.28083 20.25 6.9675C23.2917 5.65583 26.5417 5 30 5C33.4583 5 36.7083 5.65583 39.75 6.9675C42.7917 8.28083 45.4375 10.0625 47.6875 12.3125C49.9375 14.5625 51.7183 17.2083 53.03 20.25C54.3433 23.2917 55 26.5417 55 30C55 33.4583 54.3433 36.7083 53.03 39.75C51.7183 42.7917 49.9375 45.4375 47.6875 47.6875C45.4375 49.9375 42.7917 51.7183 39.75 53.03C36.7083 54.3433 33.4583 55 30 55Z" fill="#FF704E"/>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    <?php endif;?>
    </div>
</div>