/* global Splide */
const elms = document.getElementsByClassName('lively-neighborhood__splide');

const runSlider = () => {
	Array.prototype.forEach.call(elms, el => {
		new Splide(el, {
			type   : 'loop',
			perPage: 1,
			autoHeight: true,
			autoplay: true,
			speed: 1000,
			type: 'loop',
			arrows: true,
		}).mount();
	});
};

runSlider();
