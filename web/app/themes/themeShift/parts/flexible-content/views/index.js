

const switchMap = (triggers, maps) => {
	triggers.forEach(trigger => {
		trigger.addEventListener('click', e => {
			triggers.forEach(trigger => {
				e.currentTarget === trigger ? trigger.classList.add('is-active') : trigger.classList.remove('is-active');
			});
			maps.forEach(map => {
				map.dataset.map === trigger.dataset.map ? map.classList.add('show--mobile') : map.classList.remove('show--mobile');
			});

			if (trigger.dataset.map === 'micro') {
				document.querySelector('.views__micro').style.clipPath = 'inset(0 0 0 0)';
				document.querySelector('.views__line').style.left = '100%';
			} else {
				document.querySelector('.views__micro').style.clipPath = 'inset(0 100% 0 0)';
				document.querySelector('.views__line').style.left = '0%';
			}
		});
	});
};

const dragToSwitch = () => {
	const draggableLine = document.querySelector('.views__line');
	const image2 = document.querySelector('.views__micro');
	const mapsInner = document.querySelector('.views__wrapper--maps-inner');

	let isDragging = false;

	const startDrag = () => {
		isDragging = true;
	};

	const moveDrag = event => {
		if (!isDragging) return;
		const containerRect = mapsInner.getBoundingClientRect();
		const newLeft = event.type === 'mousemove' ? event.clientX - containerRect.left : event.touches[0].clientX - containerRect.left;
		const containerWidth = containerRect.width;
		let percentage = (newLeft / containerWidth) * 100;

		if (percentage <= 50)	{
			document.querySelector('.views__wrapper--buttons button[data-map="macro"]').classList.add('is-active');
			document.querySelector('.views__wrapper--buttons button[data-map="micro"]').classList.remove('is-active');
		} else {
			document.querySelector('.views__wrapper--buttons button[data-map="macro"]').classList.remove('is-active');
			document.querySelector('.views__wrapper--buttons button[data-map="micro"]').classList.add('is-active');
		}

		if (percentage < 0 || percentage > 100) {
			return;
		}
		percentage < 1 ? percentage = 0 : percentage;
		percentage > 99 ? percentage = 100 : percentage;
		draggableLine.style.left = `${percentage}%`;
		image2.style.clipPath = `inset(0 ${100 - percentage}% 0 0)`;
	};

	const endDrag = () => {
		isDragging = false;
	};

	draggableLine.addEventListener('mousedown', startDrag);
	draggableLine.addEventListener('touchstart', startDrag);


	mapsInner.addEventListener('mousemove', moveDrag);
	mapsInner.addEventListener('touchmove', moveDrag);


	document.addEventListener('mouseup', endDrag);
	document.addEventListener('touchend', endDrag);
};

const guardianOfTheMapView = mapWrapper => {
	const mediaQuery = window.matchMedia('(max-width: 769px)');

	const handleMediaQueryChange = (event) => {
		if (event.matches) {
			mapWrapper.style.setProperty('clip-path', 'inset(0px 0px 0px 0px)');
			document.querySelector('.views__wrapper--buttons button[data-map="macro"]').classList.add('is-active');
			document.querySelector('.views__wrapper--buttons button[data-map="micro"]').classList.remove('is-active');
		} else {
			mapWrapper.style.setProperty('clip-path', 'inset(0px 50% 0px 0px)');
			document.querySelector('.views__line').style.left = '50%';
		}
	};
	mediaQuery.addEventListener('change', handleMediaQueryChange);
};

document.addEventListener('DOMContentLoaded', () => {
	const switcherButtons = document.querySelectorAll('.views__wrapper--buttons button');
	const maps = document.querySelectorAll('.views__wrapper--map');
	

	switchMap(switcherButtons, maps);
	dragToSwitch();
});

// reize event
const mapWrapper = document.querySelector('.views__micro');
window.addEventListener('resize', () => {
	guardianOfTheMapView(mapWrapper);
});
