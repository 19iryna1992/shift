<?php

$macro_map          = !empty(get_sub_field('image_left')) ? get_sub_field('image_left') : 0;
$micro_right        = !empty(get_sub_field('image_right')) ? get_sub_field('image_right') : 0;
$macro_map_mobile   = !empty(get_sub_field('image_left_mobile')) ? get_sub_field('image_left_mobile') : 0;
$micro_right_mobile = !empty(get_sub_field('image_right_copy')) ? get_sub_field('image_right_copy') : 0;
$macro_map_tab      = !empty(get_sub_field('image_left_mobile_tab')) ? get_sub_field('image_left_mobile_tab') : 0;
$micro_right_tab    = !empty(get_sub_field('image_right_copy_tab')) ? get_sub_field('image_right_copy_tab') : 0;
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <?php
            load_blocks_script('views', 'shift/views', '', 'parts/flexible-content');
            load_inline_styles(__DIR__, 'views');
            ?>
            <div class="views__wrapper--buttons">
                <div class="views__wrapper--button">
                    <button id="macro_view" class="views__switcher is-active" data-map="macro">
                        <?php _e('MACRO VIEW', 'shift'); ?>
                    </button>
                </div>

                <div class="views__wrapper--button">
                    <button id="micro_view" class="views__switcher" data-map="micro">
                        <?php _e('MICRO VIEW', 'shift'); ?>
                    </button>
                </div>
            </div>
            <div class="views__wrapper--maps">
                <div class="views__wrapper--maps-inner">
                    <div class="views__wrapper--map views__macro show--mobile" data-map="macro">
                        <div class="views__map-container views__map-container--desktop">
                            <?php if ($macro_map) : ?>
                                <?php echo get_img($macro_map); ?>
                            <?php endif; ?>
                        </div>
                        <div class="views__map-container views__map-container--mobile">
                            <?php if ($macro_map_mobile) : ?>
                                <?php echo get_img($macro_map_mobile); ?>
                            <?php endif; ?>
                        </div>

                        <?php if ($macro_map_tab) : ?>
                            <a class="views__map__link" target="_blank" href="<?php echo wp_get_attachment_url($macro_map_tab); ?>">
                                <?php echo get_img('view-full'); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                    <div class="views__wrapper--map views__micro" data-map="micro">
                        <div class="views__map-container views__map-container--desktop">
                            <?php if ($micro_right) : ?>
                                <?php echo get_img($micro_right); ?>
                            <?php endif; ?>
                        </div>
                        <div class="views__map-container views__map-container--mobile">
                            <?php if ($micro_right_mobile) : ?>
                                <?php echo get_img($micro_right_mobile); ?>
                            <?php endif; ?>
                        </div>

                        <?php if ($micro_right_tab) : ?>
                            <a class="views__map__link" target="_blank" href="<?php echo wp_get_attachment_url($micro_right_tab); ?>">
                                <?php echo get_img('view-full'); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                    <div class="views__line">
                        <svg width="70" height="70" viewBox="0 0 70 70" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="35" cy="35" r="35" fill="#1F3538" />
                            <rect width="58" height="24" transform="translate(6 23)" fill="#1F3538" />
                            <path d="M21.1248 44.1L12.6998 35.7C12.5998 35.6 12.5288 35.4917 12.4868 35.375C12.4448 35.2584 12.4242 35.1334 12.4248 35C12.4248 34.8667 12.4455 34.7417 12.4868 34.625C12.5282 34.5084 12.5992 34.4 12.6998 34.3L21.1248 25.875C21.3582 25.6417 21.6498 25.525 21.9998 25.525C22.3498 25.525 22.6498 25.65 22.8998 25.9C23.1498 26.15 23.2748 26.4417 23.2748 26.775C23.2748 27.1084 23.1498 27.4 22.8998 27.65L15.5498 35L22.8998 42.35C23.1332 42.5834 23.2498 42.871 23.2498 43.213C23.2498 43.555 23.1248 43.8507 22.8748 44.1C22.6248 44.35 22.3332 44.475 21.9998 44.475C21.6665 44.475 21.3748 44.35 21.1248 44.1Z" fill="#F6F7D3" />
                            <path d="M48.8752 44.1L57.3002 35.7C57.4002 35.6 57.4712 35.4917 57.5132 35.375C57.5552 35.2584 57.5758 35.1334 57.5752 35C57.5752 34.8667 57.5545 34.7417 57.5132 34.625C57.4718 34.5084 57.4008 34.4 57.3002 34.3L48.8752 25.875C48.6418 25.6417 48.3502 25.525 48.0002 25.525C47.6502 25.525 47.3502 25.65 47.1002 25.9C46.8502 26.15 46.7252 26.4417 46.7252 26.775C46.7252 27.1084 46.8502 27.4 47.1002 27.65L54.4502 35L47.1002 42.35C46.8668 42.5834 46.7502 42.871 46.7502 43.213C46.7502 43.555 46.8752 43.8507 47.1252 44.1C47.3752 44.35 47.6668 44.475 48.0002 44.475C48.3335 44.475 48.6252 44.35 48.8752 44.1Z" fill="#F6F7D3" />
                        </svg>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>