<?php

$background_color = get_sub_field('background_color') ?: 'beige';
$title = get_sub_field('title') ?: '';
$upper_title = get_sub_field('upper_title') ?: '';
$text = get_sub_field('text') ?: '';
$count_items = get_sub_field('count_items') ?: 'four';
$icons_box = get_sub_field('icons_box') ?: [];

$classes = [];
$background_color ? $classes[] = 'points-forts__wrapper--'.$background_color.'' : null;
$count_items ? $classes[] = 'points-forts__wrapper--'.$count_items.'' : null;

?>

<div class="points-forts__wrapper <?php echo implode(' ', $classes); ?>">
    <?php load_inline_styles(__DIR__, 'points-forts'); ?>
    <div class="points-forts__herader">
    <?php if($upper_title): ?>
        <p class="points-forts__title points-forts__title--upper">
            <span class="animation fade-up">
                <span class="animation-container"><?php echo $upper_title ?></span>
            </span>
        </p>
    <?php endif; ?>
    <?php if($title): ?>
        <h2 class="points-forts__title">
            <span class="animation fade-up">
                <span class="animation-container"><?php echo $title ?></span>
            </span>
        </h2>
    <?php endif; ?>
    <?php if($text): ?>
        <div class="points-forts__text">
            <span class="animation fade-up">
                <span class="animation-container"><?php echo $text ?></span>
            </span>
        </div>
    <?php endif; ?>
    </div>
    <?php if($icons_box): ?>
    <div class="points-forts__tiles">
        <?php foreach($icons_box as $icon_box): ?>
            <?php
                $tile_ico = $icon_box['icon'] ? get_img($icon_box['icon']) : '';
                $tile_title = $icon_box['title'] ?: '';
                $tile_text = $icon_box['text'] ?: '';
            ?>
            <div class="points-forts__tile">
            <?php if($tile_ico): ?>
                <div class="points-forts__wrapper--icon animation fade-up">
                    <span class="animation-container"><?php echo $tile_ico ?></span>
                </div>
            <?php endif; ?>
            <?php if($tile_title): ?>
                <h3 class="points-forts__title points-forts__title--tile">
                    <span class="animation fade-up">
                        <span class="animation-container"><?php echo $tile_title ?></span>
                    </span>
                </h3>
            <?php endif; ?>
            <?php if($tile_text): ?>
                <div class="points-forts__text--tile">
                    <span class="animation fade-up">
                        <span class="animation-container"><?php echo $tile_text ?></span>
                    </span>
                </div>
            <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>