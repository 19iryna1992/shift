const playButtons = document.querySelectorAll('.project__media--player');

const playProjectVimeo = playButtons => {
	playButtons.forEach(playButton => {
		playButton.addEventListener('click', currentElement => {
			const container = currentElement.target.closest('.project__media--vimeo');
			if (container) {
				currentElement.currentTarget.remove();
				const iframe = container.querySelector('iframe');
				const iframeWrapper = container.closest('div');
				
				const playerOptions = {
					controls: true,
					url: iframe.src,
				};
				iframe.remove();
				const player = new Vimeo.Player(iframeWrapper.querySelector('div'), playerOptions);
				
				player.play();
			}
		});
	});
};

// dom loaded
document.addEventListener('DOMContentLoaded', () => {
	playProjectVimeo(playButtons);
});