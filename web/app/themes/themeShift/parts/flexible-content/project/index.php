<?php
$upper_title = get_sub_field('upper_title') ?: '';
$title = get_sub_field('title') ?: '';
$text = get_sub_field('text') ?: '';
$button_label = get_sub_field('section_button') ?: [];

get_sub_field('modal_shitcher') ? $button_label['modal'] = true : null;

$media_type = get_sub_field('media_type') ?: '';
$media = '';


if ($media_type === 'vimeo') {
    $media = get_sub_field('video') ? removeWidthHeight(get_sub_field('video')) : '';
} elseif ($media_type === 'image') {
    $media = get_img(get_sub_field('image_media'), 'project--thumbnail');
}

?>
<?php if($media_type === 'vimeo'): ?>
    <script src="https://player.vimeo.com/api/player.js"></script>
<?php endif; ?>

<div class="container">
    <?php
        get_sub_field('modal_shitcher') ? load_inline_styles_shared('forms') : null;
        load_inline_styles(__DIR__, 'project');
        load_blocks_script('project', 'shift/project', '', 'parts/flexible-content');
    ?>
    <div class="project__header">
        <?php if($upper_title): ?>
            <p class="project__title--upper">
                <span class="animation fade-up">
                    <span class="animation-container">
                        <?php echo $upper_title; ?>
                    </span>
                </span>
            </p>
        <?php endif; ?>
        <?php if($title): ?>
            <h2 class="project__title">
                <span class="animation fade-up">
                    <span class="animation-container">
                        <?php echo $title; ?>
                    </span>
                </span>
            </h2>
        <?php endif; ?>
    </div>

    <div class="project__content">
        <?php if($media): ?>
            <div class="animation animation__fade-left project__media project__media--<?php echo $media_type; ?>">
                <div>
                <?php echo $media; ?>
                <?php if($media_type === 'vimeo'): ?>
                    <div class="project__media--player">
                        <svg width="84" height="84" viewBox="0 0 84 84" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M33.25 32.6375V51.3625C33.25 52.7042 33.8625 53.725 35.0875 54.425C36.3125 55.125 37.5083 55.0667 38.675 54.25L53.2 44.975C54.3083 44.275 54.8625 43.2833 54.8625 42C54.8625 40.7167 54.3083 39.725 53.2 39.025L38.675 29.75C37.5083 28.9333 36.3125 28.875 35.0875 29.575C33.8625 30.275 33.25 31.2958 33.25 32.6375ZM42 77C37.1583 77 32.6083 76.0807 28.35 74.242C24.0917 72.4033 20.3875 69.9102 17.2375 66.7625C14.0875 63.6125 11.5943 59.9083 9.758 55.65C7.92167 51.3917 7.00233 46.8417 7 42C7 37.1583 7.91933 32.6083 9.758 28.35C11.5967 24.0917 14.0898 20.3875 17.2375 17.2375C20.3875 14.0875 24.0917 11.5943 28.35 9.758C32.6083 7.92167 37.1583 7.00233 42 7C46.8417 7 51.3917 7.91933 55.65 9.758C59.9083 11.5967 63.6125 14.0898 66.7625 17.2375C69.9125 20.3875 72.4068 24.0917 74.2455 28.35C76.0842 32.6083 77.0023 37.1583 77 42C77 46.8417 76.0807 51.3917 74.242 55.65C72.4033 59.9083 69.9102 63.6125 66.7625 66.7625C63.6125 69.9125 59.9083 72.4068 55.65 74.2455C51.3917 76.0842 46.8417 77.0023 42 77Z" fill="#F6F7D3" fill-opacity="0.8"/>
                        </svg>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="project__content--text">
            <?php if($text): ?>
                <div class="project__text">
                    <span class="animation fade-up">
                        <span class="animation-container">
                            <?php echo $text; ?>
                        </span>
                    </span>
                </div>
            <?php endif; ?>
            <?php if( ! empty( $button_label['section_link'] ) ) : ?>
                <div class="project__button">
                    <span class="animation fade-up">
                        <span class="animation-container">
                        <?php echo get_button($button_label); ?>
                        </span>
                    </span>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php
    if(get_sub_field('modal_shitcher')) {
        $modal_data['header'] = get_sub_field('header') ?: '';
        if(is_array(get_sub_field('image'))) {
            $img = get_sub_field('image');
            $modal_data['image'] = $img['ID'] ?: '';
        } else {
            $modal_data['image'] = get_sub_field('image') ?: '';
        }
        $modal_data['form'] = get_sub_field('form_shortcode') ?: '';
	    $modal_data['file'] = get_field('project_popup_file', 'options') ?: '';

        get_part('components/modal/index', $modal_data);
    }
?>