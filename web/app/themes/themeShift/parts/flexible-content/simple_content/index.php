<?php

$wysiwyg_editor = get_sub_field('wysiwyg_editor');
load_inline_styles(__DIR__, 'wysiwyg_editor');

?>

<?php if (!empty($wysiwyg_editor)) : ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php echo $wysiwyg_editor ?>
            </div>
        </div>
    </div>
<?php endif; ?>