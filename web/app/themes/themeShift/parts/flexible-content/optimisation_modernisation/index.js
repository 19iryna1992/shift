/* global Splide */
const elms = document.getElementsByClassName('optimisation-modernisation__splide');

const runSlider = () => {
	Array.prototype.forEach.call(elms, el => {
		new Splide(el, {
			perPage: 1,
			gap: 22,
			autoHeight: true,
			rewind: true,
			autoplay: true,
			speed: 1000,
			padding: {
				left: 22,
			},
		}).mount();
	});
};

document.addEventListener('DOMContentLoaded', runSlider);
