
const buttons = document.querySelectorAll('.wp-block-button__modal');
const closeButtons = document.querySelectorAll('.modal__close');
const forms = document.querySelectorAll('.modal__form');

const handleClick = trigger => {
	const section = trigger.closest('section');
	const modal = section.querySelector('.modal');
	modal.classList.add('is-active');

	setTimeout(() => {
		modal.classList.add('show');
	}, 150);

	document.body.classList.add('modal-open');
};

const handleClose = trigger => {
	const modal = trigger.closest('.modal');
	modal.classList.remove('show');
	setTimeout(() => {
		modal.classList.remove('is-active');
	}, 400);

	document.body.classList.remove('modal-open');
};

buttons.forEach(button => {
	button.addEventListener('click', (e) => {
		e.preventDefault();
		handleClick(e.currentTarget);
	});
});

closeButtons.forEach(button => {
	button.addEventListener('click', (e) => {
		e.preventDefault();
		handleClose(e.currentTarget);
	});
});

forms?.forEach(form => {
	const contactForm = form.querySelector('.wpcf7');
	const { file } = form.dataset;

	if (file) {
		contactForm?.addEventListener('wpcf7mailsent', () => {
			handleClose(form.parentElement.querySelector('.modal__close'));

			const element = document.createElement('a');
			element.setAttribute('href', file);
			element.setAttribute('download', '');
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);
		});
	}
});