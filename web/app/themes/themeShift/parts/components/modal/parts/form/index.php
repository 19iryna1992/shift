<button class="modal__close">
    <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M12.8 38L10 35.2L21.2 24L10 12.8L12.8 10L24 21.2L35.2 10L38 12.8L26.8 24L38 35.2L35.2 38L24 26.8L12.8 38Z" fill="#1F3538"/>
    </svg>
</button>

<div class="modal__header">
    <h2 class="modal__title"><?php echo $args['header']; ?></h2>
</div>
<?php if(isset($args['image']) && $args['image'] != ''): ?>
    <div class="modal__wrapper--image"><?php echo get_img($args['image']); ?></div>
<?php endif; ?>
<div class="modal__form" data-file="<?php echo ( ! empty( $args['file'] ) ) ? $args['file'] : ''; ?>">
    <?php echo do_shortcode($args['form']); ?>
</div>