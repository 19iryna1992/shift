
<div class="modal">
	<?php
		load_blocks_script('modal', 'theme_shift/modal');
		load_inline_styles(__DIR__, 'modal');
	?>
	<div class="modal__wrapper<?php echo isset($args['form']) ? ' modal__wrapper--form' : null; ?>">
		<?php isset($args['form']) ? get_part('components/modal/parts/form/index', $args) : get_part('components/modal/parts/iframe/index', $args); ?>
	</div>
</div>