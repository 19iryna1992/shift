<?php

$logo = get_field('footer_logo_animation', 'option');
$copyright = get_field('footer_copyright', 'option');
$link_text = get_field('footer_design_by_text', 'option');
$link = get_field('footer_design_by_link', 'option');

?>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6">
            <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
            <lottie-player src="<?php bloginfo('template_url'); ?>/parts/components/footer/shift_logo-animation_footer.json" background="transparent" speed="1" style="width: 100%; height: auto;" loop autoplay></lottie-player>
        </div>
        <div class="col-12 col-lg-6">
            <div class="main-footer__container">
                <nav class="main-footer__nav" aria-labelledby="nav-footer">
                    <span id="nav-footer" class="screen-reader-text"><?php _e('Footer Navigation', 'theme_shift'); ?></span>
                    <?php wp_nav_menu(array('theme_location' => 'foot_navigation', 'container' => false, 'walker' => new Main_Nav_Menu_Walker())); ?>
                </nav>
                <div class="main-footer__info">
                    <?php if (!empty($copyright)) : ?>
                        <span class="main-footer__copyright"><?php echo date('Y') . '©' . $copyright ?></span>
                    <?php endif; ?>
                    <?php if (!empty($link)) : ?>
                        <?php if (!empty($link_text)) : ?>
                            <div class="main-footer__info-link">
                                <span class="main-footer__link-label"><?php echo $link_text ?></span>
                                <a target="<?php echo $link['target'] ? $link['target'] : '_self'; ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>