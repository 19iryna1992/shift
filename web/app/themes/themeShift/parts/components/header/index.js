const toggleLinks = document.querySelectorAll('.btn-menu, .menu-item__dropdown');
Array.from(toggleLinks).forEach(link => {
	link.addEventListener('click', function () {
		const expanded = this.getAttribute('aria-expanded') === 'true' || false;
		this.setAttribute('aria-expanded', !expanded);
		const menu = this.nextElementSibling;
		menu.hidden = !menu.hidden;
		!menu.hidden ? document.body.style.overflow = 'hidden' : document.body.style.overflow = 'auto';
	});
});

const hoverSubmenu = document.querySelectorAll('.main-header__desktop-nav .menu-item-has-children');
Array.from(hoverSubmenu).forEach(link => {
	link.addEventListener('mouseover', function () {
		const dropdown = this.querySelector('.menu-item__dropdown');
		if (dropdown.getAttribute('aria-expanded') !== 'true') {
			const expanded = dropdown.getAttribute('aria-expanded') === 'true' || false;
			dropdown.setAttribute('aria-expanded', !expanded);
			const menu = this.querySelector('.menu-item__submenu');
			menu.hidden = false;
		}
	});
	link.addEventListener('mouseout', function () {
		const dropdown = this.querySelector('.menu-item__dropdown');
		const expanded = dropdown.getAttribute('aria-expanded') === 'true' || false;
		dropdown.setAttribute('aria-expanded', !expanded);
		const menu = this.querySelector('.menu-item__submenu');
		menu.hidden = true;
	});
});

export const setCookie = (cname, cvalue, exdays) => {
	const date = new Date();
	date.setTime(date.getTime() + (exdays * 24 * 60 * 60 * 1000));
	const expires = `expires=${date.toUTCString()}`;
	document.cookie = `${cname}=${cvalue};${expires};path=/`;
};

const langLinks = document.querySelectorAll('.wpml-ls-link');

if (langLinks) {
	langLinks.forEach(link => {
		link.addEventListener('click', () => {
			setCookie('changed_language', '1');
		});
	});
}

document.addEventListener('DOMContentLoaded', () => {
	if (document.body.classList.contains('changed-language')) {
		setCookie('changed_language', '0');

		document.body.classList.add('loaded');
	}

	const menuLinks = document.querySelectorAll('.main-header__mobile-nav a');

	menuLinks.forEach(link => {
		link.addEventListener('click', () => {
			document.body.style.overflow = 'auto';
			document.querySelector('.main-header__mobile').hidden = true;
			document.querySelector('.btn-menu').setAttribute('aria-expanded', false);
		});
	});
});