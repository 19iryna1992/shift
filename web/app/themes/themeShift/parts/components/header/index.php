<?php
$main_logo = get_field('header_logo', 'option'); // ACF ID // icon name

load_blocks_script('header', 'theme_shift/header');
?>

<header class="main-header">
	<?php load_inline_styles(__DIR__, 'header'); ?>
	<div class="main-header__logo">
		<a href="<?php echo get_bloginfo('url'); ?>" class="main-logo js--main-logo"><span class="screen-reader-text"><?php _e('Main Logo', 'theme_shift'); ?></span><?php echo get_img($main_logo, 'full') ?></a>
	</div>
	<?php if (has_nav_menu('navigation')) : ?>
		<div class="main-header__nav">
			<nav class="main-header__desktop-nav" aria-labelledby="nav-heading">
				<span id="nav-heading" class="screen-reader-text"><?php _e('Main Navigation', 'theme_shift'); ?></span>
				<?php wp_nav_menu(array('theme_location' => 'navigation', 'container' => false, 'walker' => new Main_Nav_Menu_Walker())); ?>
				<div class="main-header__lang">
					<?php echo do_shortcode('[wpml_language_selector_widget]') ?>
				</div>
			</nav>
		</div>
	<?php endif; ?>
	<button class="btn-menu" aria-haspopup="true" aria-expanded="false">
		<span class="btn-menu__icon btn-menu__open"><?php echo get_img('menu-burger', 'full') ?></span>
		<span class="btn-menu__icon btn-menu__close"><?php echo get_img('menu-close', 'full') ?></span>
		<span class="screen-reader-text"><?php _e('Menu', 'theme_shift'); ?></span>
	</button>
	<div class="main-header__mobile" hidden>
		<nav class="main-header__mobile-nav" aria-labelledby="mobile-nav-heading">
			<span id="mobile-nav-heading" class="screen-reader-text"><?php _e('Mobile Navigation', 'theme_shift'); ?></span>
			<?php wp_nav_menu(array('theme_location' => 'navigation', 'container' => false, 'walker' => new Main_Nav_Menu_Walker())); ?>
			<div class="main-header__lang">
				<?php echo do_shortcode('[wpml_language_selector_widget]') ?>
			</div>
		</nav>
	</div>
</header>

<?php

if ( is_front_page() ) {
	if ( empty( $_COOKIE['changed_language'] ) ) {
		echo get_part('components/preloader/index');
	}
}
