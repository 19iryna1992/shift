const startTime = performance.now();

window.onload = function () {
	const loadTime = performance.now() - startTime;
	const initialDelay = 5000;

	const animationDelay = initialDelay - loadTime;

	const preloader = document.querySelector('.js--preloader-animation');

	preloader.style.position = 'fixed';

	window.setTimeout(() => {
		document.body.classList.add('loaded');
	}, animationDelay);
};

