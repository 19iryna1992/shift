<div class="preloader">
    <?php
    load_blocks_script('preloader', 'theme_shift/preloader');
    load_inline_styles(__DIR__, 'preloader');
    ?>

    <div class="preloader__animation js--preloader-animation">
        <lottie-player src="<?php bloginfo('template_url'); ?>/parts/components/preloader/shift_logo_animation.json" background="transparent" speed="1" autoplay></lottie-player>
    </div>

</div>