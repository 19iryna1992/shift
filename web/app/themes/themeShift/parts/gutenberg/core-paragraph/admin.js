const { __ } = wp.i18n;
const { registerBlockStyle } = wp.blocks;

const styles = [
	{
		name: 'uppercase',
		label: __('Uppercase', 'theme_shift'),
	},
	{
		name: 'subheading',
		label: __('Subheading', 'theme_shift'),
	},
	{
		name: 'leadparagraph',
		label: __('Leadparagraph', 'theme_shift'),
	},
];

wp.domReady(() => {
	styles.forEach(style => {
		registerBlockStyle('core/paragraph', style);
	});
});
